FROM python:3.5
RUN curl -sSL https://get.docker.com/ | sh
RUN apt-get -y update && apt-get -y install python-dev python-pip python-lxml libgdal-dev python3-gdal && apt-get -y clean
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt
COPY . /usr/src/app
