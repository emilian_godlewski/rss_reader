from time import mktime
from datetime import datetime

import feedparser
from django.db import models


class CurrencyEntry(models.Model):
    date_added = models.DateTimeField()
    currency = models.ForeignKey('main.CurrencyRSSSource', on_delete=models.CASCADE)
    url = models.URLField()
    exchange_ratio = models.FloatField()


class CurrencyRSSSource(models.Model):
    name = models.CharField(max_length=64)
    currency_code = models.CharField(max_length=3)

    url = models.URLField()

    def __unicode__(self):
        return self.name

    def parse_exchange_ratio(self, exchange_ratio):
        return exchange_ratio.strip('\nEUR')

    def scrap(self):
        feed = feedparser.parse(self.url)
        for item in feed['items']:
            CurrencyEntry.objects.get_or_create(
                currency=self,
                url=item['link'],
                date_added=datetime.fromtimestamp(mktime(item['updated_parsed'])),
                defaults={
                    'exchange_ratio': self.parse_exchange_ratio(item['cb_exchangerate']),
                }
            )
