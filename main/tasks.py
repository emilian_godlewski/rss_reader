from rss_reader.celery import app

from .models import CurrencyRSSSource


@app.task()
def scrap_rss():
    for source in CurrencyRSSSource.objects.all():
        source.scrap()
