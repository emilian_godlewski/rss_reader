from django.shortcuts import get_object_or_404
from rest_framework import mixins, viewsets
from rest_framework.response import Response

from .models import CurrencyEntry, CurrencyRSSSource
from .serializers import CurrencyEntrySerializer


class CurrencyEntryViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = CurrencyEntry.objects.all()
    serializer_class = CurrencyEntrySerializer

    def list(self, request, *args, **kwargs):
        currency_code = self.request.GET.get('currency')
        currency = get_object_or_404(CurrencyRSSSource, currency_code=currency_code)

        queryset = (
            self.get_queryset()
            .filter(currency=currency)
            .order_by('-date_added')
        )

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
