from rest_framework import serializers

from .models import CurrencyEntry


class CurrencyEntrySerializer(serializers.ModelSerializer):
    currency = serializers.SerializerMethodField()

    class Meta(object):
        model = CurrencyEntry
        fields = (
            'currency',
            'date_added',
            'exchange_ratio',
        )

    def get_currency(self, obj):
        return obj.currency.name
