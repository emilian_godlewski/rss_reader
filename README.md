# RSS_reader

To setup project just use 
    
    docker-compose up

The project will start, then open another terminal and use

    docker-compose exec web /bin/bash
    python manage.py migrate
    python manage.py createsuperuser
    
And login to admin panel and create sample CurrencyRSSSource.
App will download exchange ratios and save them in db. You can then
access them with /api/ratios?currency=<currency_code> endpoint.
