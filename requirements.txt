Celery==4.1.1
Django==2.0.3
django-extensions==2.0.7
djangorestframework==3.7.7
django-redis==4.9.0
feedparser==5.2.1
psycopg2==2.6.2
redis==2.10.6
