from __future__ import absolute_import, unicode_literals

import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'rss_reader.settings')

from django.conf import settings

app = Celery('rss_reader')
app.conf.broker_url = 'redis://redis:6379/0'
app.conf.broker_transport_options = {'visibility_timeout': 3600}
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
app.conf.beat_schedule = {
    'scrap_rss': {
        'task': 'main.tasks.scrap_rss',
        'schedule': 60.0,
        'args': [],
    },
}
