from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers

from main.views import CurrencyEntryViewSet

router = routers.DefaultRouter()
router.register(r'ratios', CurrencyEntryViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls))
]
